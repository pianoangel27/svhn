from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import os.path
import time

import numpy as np
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf

import svhn_googlenet

FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string('train_dir', '/home/verbal/svhn/ggnet', """Directory where to write event logs and checkpoint.""")
# Changed 100000 to 1000
tf.app.flags.DEFINE_integer('max_steps', 600, """Number of batches to run.""")
tf.app.flags.DEFINE_boolean('log_device_placement', False, """Whether to log device placement.""")



def train():
  """Train SVHN for a number of steps."""
  with tf.Graph().as_default():
    global_step = tf.Variable(0, trainable=False)

    # Get images and labels for SVHN.
    images, labels = svhn_googlenet.inputs('svhn_data/train_32x32.mat')
    # Build a Graph that computes the logits predictions from the
    # inference model.
    logits = svhn_googlenet.inference(images)

    # Calculate loss.
    loss = svhn_googlenet.loss(logits, labels)

    # Build a Graph that trains the model with one batch of examples and
    # updates the model parameters.
    train_op = svhn_googlenet.train(loss, global_step)

    # Create a saver.
    saver = tf.train.Saver(tf.all_variables())
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=FLAGS.log_device_placement))
#    with tf.Session() as sess:
    # Build an initialization operation to run below.
    init = tf.initialize_all_variables()
    sess.run(init)
    
    # Start running operations on the Graph.
    ckpt = tf.train.get_checkpoint_state(FLAGS.train_dir)

    if ckpt and ckpt.model_checkpoint_path:
    #if  os.path.isfile("/home/verbal/svhn/svhn_train/model.ckpt-1000"):
      # Restore variables from disk.
        saver.restore(sess,ckpt.model_checkpoint_path)
        print("Model restored.")

    # Build the summary operation based on the TF collection of Summaries.
    summary_op = tf.merge_all_summaries()

    # Start the queue runners.
    tf.train.start_queue_runners(sess=sess)

    summary_writer = tf.train.SummaryWriter(FLAGS.train_dir, graph_def=sess.graph_def)

    for step in xrange(FLAGS.max_steps):
      start_time = time.time()
      _, loss_value = sess.run([train_op, loss])
      duration = time.time() - start_time

      assert not np.isnan(loss_value), 'Model diverged with loss = NaN'

      if step % 10 == 0:
        num_examples_per_step = FLAGS.batch_size
        examples_per_sec = num_examples_per_step / duration
        sec_per_batch = float(duration)

        format_str = ('%s: step %d, loss = %.2f (%.1f examples/sec; %.3f '
                      'sec/batch)')
        print (format_str % (datetime.now(), step, loss_value,
                             examples_per_sec, sec_per_batch))

      if step % 100 == 0:
        summary_str = sess.run(summary_op)
        summary_writer.add_summary(summary_str, step)

      # Save the model checkpoint periodically.
      #if step % 1000 == 0 or (step + 1) == FLAGS.max_steps:
        checkpoint_path = os.path.join(FLAGS.train_dir, 'model.ckpt')
        saver.save(sess, checkpoint_path, global_step=step)
      tf.train.write_graph(sess.graph_def, '/home/verbal/svhn/ggnet', 'train.pb',as_text=False)


def main(argv=None):  # pylint: disable=unused-argument
  #svhn.maybe_download_and_extract()
  '''
  if tf.gfile.Exists(FLAGS.train_dir):
    tf.gfile.DeleteRecursively(FLAGS.train_dir)
  tf.gfile.MakeDirs(FLAGS.train_dir)
  '''
  train()

if __name__ == '__main__':
  tf.app.run()
