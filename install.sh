#!/bin/sh

#Ubuntu Installer Script

sudo apt-get update
sudo apt-get install build-essential python python-dev 
sudo apt-get install python-pip

sudo pip install --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.7.1-cp27-none-linux_x86_64.whl

sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer

wget https://github.com/bazelbuild/bazel/releases/download/0.2.0/bazel_0.2.0-linux-x86_64.deb 
export PATH="$PATH:$HOME/bin"

sudo apt-get install git
git clone --recurse-submodules https://github.com/tensorflow/tensorflow

wget https://storage.googleapis.com/download.tensorflow.org/models/inception5h.zip -O /tmp/inception5h.zip


#Android SDK
mkdir ~/android
cd ~/android
wget http://dl.google.com/android/android-sdk_r24.4.1-linux.tgz
tar -xvf android-sdk_r24.4.1-linux.tgz 

#Android NDK 
wget http://dl.google.com/android/ndk/android-ndk-r10e-linux-x86_64.bin
chmod a+x *.bin
./android-ndk-r10e-linux-x86_64.bin

# Install sdk platform tools
cd ~/android/android-sdk-linux
tools/android update sdk --no-ui
